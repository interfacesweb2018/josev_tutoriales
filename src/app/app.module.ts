import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InstalationComponent } from './instalation/instalation.component';
import { StructureComponent } from './structure/structure.component';
import { ArtisanComponent } from './artisan/artisan.component';
import { MVCComponent } from './mvc/mvc.component';
import { ApiRestComponent } from './api-rest/api-rest.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InstalationComponent,
    StructureComponent,
    ArtisanComponent,
    MVCComponent,
    ApiRestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
