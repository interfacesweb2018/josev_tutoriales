import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MVCComponent } from './mvc.component';

describe('MVCComponent', () => {
  let component: MVCComponent;
  let fixture: ComponentFixture<MVCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MVCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MVCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
