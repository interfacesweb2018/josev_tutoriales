import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InstalationComponent } from './instalation/instalation.component';
import { StructureComponent } from './structure/structure.component';
import { ArtisanComponent } from './artisan/artisan.component';
import { MVCComponent } from './mvc/mvc.component';
import { ApiRestComponent } from './api-rest/api-rest.component';


const routes: Routes = [
  { path: 'instalation' ,component: InstalationComponent},
  { path: 'structure' ,component: StructureComponent},
  { path: 'artisan' ,component: ArtisanComponent},
  { path: 'mvc' ,component: MVCComponent},
  { path: 'api' ,component: ApiRestComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
